#include <iostream>
#include <set>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iterator>
#include <string>

using namespace std;

int main() {
	ifstream words;
	words.open("words.txt");

	typedef multiset<string> wordSet;
	typedef multiset<string>::iterator wordSetIt;

	wordSet WordSet;

	string word;
	while (words >> word) {
		WordSet.insert(word);
	}

	for (auto word = WordSet.begin(); word != WordSet.end(); word = WordSet.upper_bound(*word)){
		cout << *word << ":" << WordSet.count(*word) << endl;
	}

	system("pause");
	return 0;
}