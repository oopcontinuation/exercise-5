#include <iostream>
#include <set>
#include <functional>
#include <iterator>
#include <string>

using namespace std;

int main() {
	set<char, less<char>> charSet;
	string line;

	cout << "Input characters\n";
	getline(cin, line);

	string::iterator it;
	for (it = line.begin(); it < line.end(); it++) {
		charSet.insert(*it);
	}


	set<char, less<char>>::iterator charSetIterator;
	for (charSetIterator = charSet.begin(); charSetIterator != charSet.end(); charSetIterator++){
		cout << *charSetIterator << endl;
	}

	system("pause");
	return 0;
}