#include <iostream>
#include <map>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iterator>
#include <string>

using namespace std;

int main() {
	ifstream words;
	words.open("words.txt");

	map<string, int> wordMap;

	string word;
	while (words >> word) {
		wordMap[word]++;
	}
	
	for (auto word = wordMap.begin(); word != wordMap.end(); word++) {
		cout << word->first << ":" << word->second << endl;
	}
	
	system("pause");
	return 0;
}